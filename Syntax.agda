module Syntax where

open import Data.Fin
  hiding
    ( _+_ )
open import Data.Nat
open import Data.Vec

--------------------------------------------------------------------------------
-- Types for terms
--------------------------------------------------------------------------------

infixr 5 _⇒_
data Type : Set where
  Bool : Type
  _⇒_  : (τ₁ τ₂ : Type) → Type

infix  1 _∶_∷_ 
infixl 9 _·_
infixr 0 def_⇒_
mutual
  data Tel (n : ℕ) : ℕ → Set where
    []    : Tel n 0
    _∶_∷_ : ∀ {m} → (t : Chk (m + n)) → (τ : Type) → (Δ : Tel n m) → Tel n (suc m)

  ------------------------------------------------------------------------------
  -- Well-scoped type inferrable terms
  ------------------------------------------------------------------------------

  data Inf (n : ℕ) : Set where
    ff            : Inf n
    tt            : Inf n
    if_then_else_ : (t₁ : Chk n) → (t₂ t₃ : Inf n) → Inf n
    var           : (x : Fin n) → Inf n
    _·_           : (t₁ : Inf n) → (t₂ : Chk n) → Inf n
    def_⇒_        : ∀ {m} → (Δ : Tel n m) → (t : Inf (m + n)) → Inf n

  ------------------------------------------------------------------------------
  -- Well-scoped type checkable terms
  ------------------------------------------------------------------------------

  data Chk (n : ℕ) : Set where
    ƛ             : (t : Chk (suc n)) → Chk n
    inf           : (t : Inf n) → Chk n

--------------------------------------------------------------------------------
-- Modes; allows data/funs to be parametric wrt to the type of terms
--------------------------------------------------------------------------------

data Mode : Set where
  inf : Mode
  chk : Mode

term : Mode → ℕ → Set
term inf = Inf
term chk = Chk
