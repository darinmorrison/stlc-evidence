module Semantics.Static.Proof where

open import Data.Nat
open import Data.Product
open import Data.Sum
open import Data.Vec
open import Function
  hiding
    ( _∶_ )
open import Relation.Binary.PropositionalEquality
open import Relation.Nullary

open import Semantics.Static.Specification
open import Syntax

--------------------------------------------------------------------------------
-- Injectivity of type constructors
--------------------------------------------------------------------------------

⇒-inj₁ : ∀ {τ₁ τ₂ τ₁′ τ₂′} → τ₁ ⇒ τ₂ ≡ τ₁′ ⇒ τ₂′ → τ₁ ≡ τ₁′
⇒-inj₁ refl = refl

⇒-inj₂ : ∀ {τ₁ τ₂ τ₁′ τ₂′} → τ₁ ⇒ τ₂ ≡ τ₁′ ⇒ τ₂′ → τ₂ ≡ τ₂′
⇒-inj₂ refl = refl

--------------------------------------------------------------------------------
-- Decidable equality for Types
--------------------------------------------------------------------------------

_≟-Type_ : (τ₁ τ₂ : Type) → τ₁ ≡ τ₂ ⊎ ¬ (τ₁ ≡ τ₂)
Bool      ≟-Type Bool        = inj₁ refl
Bool      ≟-Type (_   ⇒ _  ) = inj₂ λ()
(_  ⇒ _ ) ≟-Type Bool        = inj₂ λ()
(τ₁ ⇒ τ₂) ≟-Type (τ₁′ ⇒ τ₂′) with τ₁ ≟-Type τ₁′
(._ ⇒ τ₂) ≟-Type (τ₁  ⇒ τ₂′)
  | inj₁ refl                with τ₂ ≟-Type τ₂′
(._ ⇒ ._) ≟-Type (τ₁  ⇒ τ₂ )
  | inj₁ refl
  | inj₁ refl                = inj₁ refl
(._ ⇒ τ₂) ≟-Type (τ₁  ⇒ τ₂′)
  | inj₁ refl
  | inj₂ τ₂≡τ₂′              = inj₂ (τ₂≡τ₂′ ∘ ⇒-inj₂)
(τ₁ ⇒ τ₂) ≟-Type (τ₁′ ⇒ τ₂′)
  | inj₂ τ₁≡τ₁′              = inj₂ (τ₁≡τ₁′ ∘ ⇒-inj₁)

mutual
  ------------------------------------------------------------------------------
  -- Telescope checking is decidable
  ------------------------------------------------------------------------------

  checkTel : ∀ {n m} (Γ : Ctx n) (Δ : Tel n m)
           → (Γ ⊢ Δ) ⊎ ¬ (Γ ⊢ Δ)
  checkTel Γ []          = inj₁ []
  checkTel Γ (t ∶ τ ∷ Δ) with check (Δ ⊕Ctx Γ) t τ
  checkTel Γ (t ∶ τ ∷ Δ)
    | inj₁  ct           with checkTel Γ Δ
  checkTel Γ (t ∶ τ ∷ Δ)
    | inj₁  ct
    | inj₁  Γ⊢Δ = inj₁ (ct ∷ Γ⊢Δ)
  checkTel Γ (t ∶ τ ∷ Δ)
    | inj₁  ct
    | inj₂ ¬Γ⊢Δ = inj₂ (¬Γ⊢Δ ∘ inv)
    where
      inv : ∀ {n m} {Γ : Ctx n} {Δ : Tel n m} {t τ}
          → Γ ⊢ t ∶ τ ∷ Δ
          → Γ ⊢ Δ
      inv (_ ∷ Γ⊢Δ) = Γ⊢Δ
  checkTel Γ (t ∶ τ ∷ Δ)
    | inj₂ ¬ct  = inj₂ (¬ct ∘ inv)
    where
      inv : ∀ {n m} {Γ : Ctx n} {Δ : Tel n m} {t τ}
          → Γ ⊢ t ∶ τ ∷ Δ
          → Δ ⊕Ctx Γ ⊢[ chk ] t ∶ τ
      inv (ct ∷ _) = ct

  ------------------------------------------------------------------------------
  -- Type inference is deterministic
  ------------------------------------------------------------------------------

  inf-determinacy : ∀ {n} {Γ : Ctx n} {t τ τ′}
                  → Γ ⊢[ inf ] t ∶ τ
                  → Γ ⊢[ inf ] t ∶ τ′
                  → τ′ ≡ τ
  inf-determinacy ff           ff             = refl
  inf-determinacy tt           tt             = refl
  inf-determinacy (if _ then it₂  else _ ∣ _)
                  (if _ then it₂′ else _ ∣ _) = inf-determinacy it₂ it₂′
  inf-determinacy (var p-≡)    (var p-≡′)     = trans (sym p-≡′) p-≡
  inf-determinacy (it₁ · _)    (it₁′ · _)     with inf-determinacy it₁ it₁′
  ... | refl                                  = refl
  inf-determinacy (def _ ⇒ it) (def _ ⇒ it′)  = inf-determinacy it it′

  ------------------------------------------------------------------------------
  -- Type inference is decidable
  ------------------------------------------------------------------------------

  infer : ∀ {n} (Γ : Ctx n) t
        →   (∃ λ τ → Γ ⊢[ inf ] t ∶ τ)
        ⊎ ¬ (∃ λ τ → Γ ⊢[ inf ] t ∶ τ)
  infer Γ ff                                             = inj₁ (, ff)
  infer Γ tt                                             = inj₁ (, tt)
  infer Γ (if t₁ then t₂ else t₃)                        with check Γ t₁ Bool
  infer Γ (if t₁ then t₂ else t₃)
    | inj₁       ct₁                                     with infer Γ t₂
  infer Γ (if t₁ then t₂ else t₃)
    | inj₁       it₁
    | inj₁       it₂                                     with infer Γ t₃
  infer Γ (if t₁ then t₂ else t₃)
    | inj₁       ct₁
    | inj₁ (τ  , it₂)
    | inj₁ (τ′ , it₃)                                    with τ′ ≟-Type τ
  infer Γ (if t₁ then t₂ else t₃)
    | inj₁       ct₁ 
    | inj₁ (τ ,  it₂)
    | inj₁ (._ , it₃)
    | inj₁ refl                                          = inj₁ (, if ct₁ then it₂ else it₃ ∣ refl)
  infer Γ (if t₁ then t₂ else t₃)
    | inj₁       ct₁
    | inj₁ (τ ,  it₂)
    | inj₁ (τ′ , it₃)
    | inj₂ ¬τ′≡τ                                         = inj₂ (¬τ′≡τ ∘ lemma it₂ it₃)
    where
      lemma : ∀ {n} {Γ : Ctx n} {t₁ t₂ t₃} {τ τ′}
            → Γ ⊢[ inf ] t₂ ∶ τ
            → Γ ⊢[ inf ] t₃ ∶ τ′
            → (∃ λ τ″ → Γ ⊢[ inf ] if t₁ then t₂ else t₃ ∶ τ″)
            → τ′ ≡ τ
      lemma it₂ it₃ (_ , if ct₁′ then it₂′ else it₃′ ∣ refl) = trans (inf-determinacy it₃′ it₃) (inf-determinacy it₂ it₂′)
  infer Γ (if t₁ then t₂ else t₃)
    | inj₁      ct₁
    | inj₁ (τ , it₂)
    | inj₂ ¬it₃                                          = inj₂ (¬it₃ ∘ inv)
    where
      inv : ∀ {n} {Γ : Ctx n} {t₁ t₂ t₃}
          → (∃ λ τ → Γ ⊢[ inf ] if t₁ then t₂ else t₃ ∶ τ)
          → (∃ λ τ → Γ ⊢[ inf ] t₃ ∶ τ)
      inv (_ , if d₁ then d₂ else it₃ ∣ τ′≡τ) = , it₃
  infer Γ (if t₁ then t₂ else t₃)
    | inj₁  ct₁
    | inj₂ ¬it₂                                          = inj₂ (¬it₂ ∘ inv)
    where
      inv : ∀ {n} {Γ : Ctx n} {t₁ t₂ t₃}
          → (∃ λ τ → Γ ⊢[ inf ] if t₁ then t₂ else t₃ ∶ τ)
          → (∃ λ τ → Γ ⊢[ inf ] t₂ ∶ τ)
      inv (_ , if d₁ then d₂ else d₃ ∣ _) = , d₂
  infer Γ (if t₁ then t₂ else t₃)
    | inj₂ ¬ct₁                                          = inj₂ (¬ct₁ ∘ inv ∘ proj₂)
    where
      inv : ∀ {n} {Γ : Ctx n} {t₁ t₂ t₃ τ}
          → Γ ⊢[ inf ] if t₁ then t₂ else t₃ ∶ τ
          → Γ ⊢[ chk ] t₁ ∶ Bool
      inv (if d₁ then d₂ else d₃ ∣ _) = d₁
  infer Γ (var x)                                        = inj₁ (, var refl)
  infer Γ (t₁ · t₂)                                      with infer Γ t₁
  infer Γ (t₁ · t₂)
    | inj₁ (Bool , it₁) = inj₂ (lemma₂ ∘ inf-determinacy it₁ ∘ proj₂ ∘ proj₂ ∘ lemma₁)
    where
      lemma₁ : ∀ {n} {Γ : Ctx n} {t₁ t₂}
             → (∃ λ τ → Γ ⊢[ inf ] t₁ · t₂ ∶ τ)
             → (∃ λ τ₁ → ∃ λ τ₂ → Γ ⊢[ inf ] t₁ ∶ τ₁ ⇒ τ₂)
      lemma₁ (_ , (d₁ · d₂)) = , (, d₁)

      lemma₂ : ∀ {τ₁ τ₂}
             → ¬ (τ₁ ⇒ τ₂ ≡ Bool)
      lemma₂ ()
  infer Γ (t₁ · t₂)
    | inj₁ (τ₁ ⇒ τ₂ , it₁) with check Γ t₂ τ₁
  infer Γ (t₁ · t₂)
    | inj₁ (τ₁ ⇒ τ₂ , it₁)
    | inj₁ ct₂                                           = inj₁ (, (it₁ · ct₂))
  infer Γ (t₁ · t₂)
    | inj₁ (τ₁ ⇒ τ₂ , it₁)
    | inj₂ ct₂                                           = inj₂ (ct₂ ∘ lemma it₁)
    where
      lemma : ∀ {n} {Γ : Ctx n} {t₁ t₂} {τ₁ τ₂}
            → Γ ⊢[ inf ] t₁ ∶ τ₁ ⇒ τ₂
            → (∃ λ τ → Γ ⊢[ inf ] t₁ · t₂ ∶ τ)
            → Γ ⊢[ chk ] t₂ ∶ τ₁
      lemma d₁ (_ , (d₁′ · d₂)) with inf-determinacy d₁ d₁′
      ... | refl = d₂
  infer Γ (t₁ · t₂)                                      
    | inj₂ ¬it₁                                          = inj₂ (¬it₁ ∘ inv)
    where
      inv : ∀ {n} {Γ : Ctx n} {t₁ t₂}
          → (∃ λ τ → Γ ⊢[ inf ] t₁ · t₂ ∶ τ)
          → (∃ λ τ → Γ ⊢[ inf ] t₁ ∶ τ)
      inv (τ , (d₁ · d₂)) = , d₁
  infer Γ (def Δ ⇒ t)                                    with infer (Δ ⊕Ctx Γ) t
  infer Γ (def Δ ⇒ t)
    | inj₁  (_ , it)                                     with checkTel Γ Δ
  infer Γ (def Δ ⇒ t)
    | inj₁  (_ , it)
    | inj₁  Γ⊢Δ                                          = inj₁ (, (def Γ⊢Δ ⇒ it))
  infer Γ (def Δ ⇒ t)
    | inj₁  (_ , it)
    | inj₂ ¬Γ⊢Δ                                          = inj₂ (¬Γ⊢Δ ∘ inv ∘ proj₂)
    where
      inv : ∀ {n m} {Γ : Ctx n} {Δ : Tel n m} {t τ}
          → Γ ⊢[ inf ] def Δ ⇒ t ∶ τ
          → Γ ⊢ Δ
      inv (def Γ⊢Δ ⇒ it) = Γ⊢Δ
  infer Γ (def Δ ⇒ t)
    | inj₂ ¬it                                           = inj₂ (λ x → ¬it (inv x))
    where
      inv : ∀ {n m} {Γ : Ctx n} {Δ : Tel n m} {t}
          → ∃ (λ τ → Γ ⊢[ inf ] def Δ ⇒ t ∶ τ)
          → ∃ (λ τ → Δ ⊕Ctx Γ ⊢[ inf ] t ∶ τ)
      inv (_ , def ⊢Δ ⇒ it) = , it

  ------------------------------------------------------------------------------
  -- Type checking is decidable
  ------------------------------------------------------------------------------

  check : ∀ {n} (Γ : Ctx n) t τ
        →    Γ ⊢[ chk ] t ∶ τ
        ⊎ ¬ (Γ ⊢[ chk ] t ∶ τ)
  check Γ (ƛ t) Bool                              = inj₂ λ()
  check Γ (ƛ t) (τ₁ ⇒ τ₂)                         with check (τ₁ ∷ Γ) t τ₂
  ... | inj₁  ct                                  = inj₁ (ƛ ct)
  ... | inj₂ ¬ct                                  = inj₂ (¬ct ∘ inv)
    where
      inv : ∀ {n} {Γ : Ctx n} {t : Chk (suc n)} {τ₁ τ₂ : Type}
          → Γ ⊢[ chk ] ƛ t ∶ τ₁ ⇒ τ₂
          → τ₁ ∷ Γ ⊢[ chk ] t ∶ τ₂
      inv (ƛ t) = t
  check Γ (inf t) τ                               with infer Γ t
  check Γ (inf t) τ | inj₁ (τ′ , it)              with τ′ ≟-Type τ
  check Γ (inf t) τ | inj₁ (._ , it) | inj₁  refl = inj₁ (inf it refl)
  check Γ (inf t) τ | inj₁ (τ′ , it) | inj₂ ¬τ′≡τ = inj₂ (¬τ′≡τ ∘ inv it)
    where
      inv : ∀ {n} {Γ : Ctx n} {t} {τ τ′}
          → Γ ⊢[ inf ] t ∶ τ
          → Γ ⊢[ chk ] inf t ∶ τ′
          → τ ≡ τ′
      inv it (inf it′ refl) = sym (inf-determinacy it it′)
  check Γ (inf t) τ | inj₂ ¬it                    = inj₂ (¬it ∘ inv)
    where
      inv : ∀ {n} {Γ : Ctx n} {t} {τ}
          → Γ ⊢[ chk ] inf t ∶ τ
          → (∃ λ τ → Γ ⊢[ inf ] t ∶ τ)
      inv (inf it _) = , it
