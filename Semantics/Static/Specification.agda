module Semantics.Static.Specification where

open import Data.Fin
  hiding
    ( _+_ )
open import Data.Nat
open import Data.Product
open import Data.Vec
open import Relation.Binary.PropositionalEquality

open import Syntax

Ctx : ℕ → Set
Ctx n = Vec Type n

infixr 5 _⊕Ctx_
_⊕Ctx_ : ∀ {n m} → Tel n m → Ctx n → Ctx (m + n)
[]          ⊕Ctx Γ = Γ
(t ∶ τ ∷ Δ) ⊕Ctx Γ = τ ∷ Δ ⊕Ctx Γ

lookupType : ∀ {n} → Fin n → Ctx n → Type
lookupType = lookup

--------------------------------------------------------------------------------
-- Specification for type inference and type checking
--------------------------------------------------------------------------------

mutual
  infix 0 _⊢_
  data _⊢_ : ∀ {n m} → Ctx n → Tel n m → Set where
    []  : ∀ {n} {Γ : Ctx n}
        → Γ ⊢ []

    _∷_ : ∀ {n m} {Γ : Ctx n} {Δ : Tel n m} {t} {τ}
        → (ct : Δ ⊕Ctx Γ ⊢[ chk ] t ∶ τ)
        → (⊢Δ : Γ ⊢ Δ)
        → Γ ⊢ t ∶ τ ∷ Δ

  infix 5 _⊢[_]_∶_
  data _⊢[_]_∶_ : ∀ {n} → Ctx n → (μ : Mode) → term μ n → Type → Set where
    ff              : ∀ {n} {Γ : Ctx n}
                    → Γ ⊢[ inf ] ff ∶ Bool

    tt              : ∀ {n} {Γ : Ctx n}
                    → Γ ⊢[ inf ] tt ∶ Bool

    if_then_else_∣_ : ∀ {n} {Γ : Ctx n} {t₁ t₂ t₃} {τ τ′}
                    → (ct₁  : Γ ⊢[ chk ] t₁ ∶ Bool)
                    → (it₂  : Γ ⊢[ inf ] t₂ ∶ τ )
                    → (it₃  : Γ ⊢[ inf ] t₃ ∶ τ′)
                    → (τ′≡τ : τ′ ≡ τ)
                    → Γ ⊢[ inf ] if t₁ then t₂ else t₃ ∶ τ

    var             : ∀ {n} {x : Fin n} {Γ : Ctx n} {τ}
                    → (p-≡ : lookupType x Γ ≡ τ)
                    → Γ ⊢[ inf ] var x ∶ τ

    _·_             : ∀ {n} {Γ : Ctx n} {t₁ t₂} {τ₁ τ₂}
                    → (it₁ : Γ ⊢[ inf ] t₁ ∶ τ₁ ⇒ τ₂)
                    → (ct₂ : Γ ⊢[ chk ] t₂ ∶ τ₁)
                    → Γ ⊢[ inf ] t₁ · t₂ ∶ τ₂

    def_⇒_          : ∀ {n m} {Γ : Ctx n} {Δ : Tel n m} {t : Inf (m + n)} {τ}
                    → (⊢Δ : Γ ⊢ Δ)
                    → (it : Δ ⊕Ctx Γ ⊢[ inf ] t ∶ τ)
                    → Γ ⊢[ inf ] def Δ ⇒ t ∶ τ

    ƛ               : ∀ {n} {Γ : Ctx n} {t} {τ₁ τ₂}
                    → (ct : τ₁ ∷ Γ ⊢[ chk ] t ∶ τ₂)
                    → Γ ⊢[ chk ] ƛ t ∶ τ₁ ⇒ τ₂

    inf             : ∀ {n} {Γ : Ctx n} {t} {τ τ′}
                    → (it   : Γ ⊢[ inf ] t ∶ τ′)
                    → (τ′≡τ : τ′ ≡ τ)
                    → Γ ⊢[ chk ] inf t ∶ τ

--------------------------------------------------------------------------------
-- Inversion principles
--------------------------------------------------------------------------------

-- inv-var : ∀ {n} {x : Fin n} {Γ : Ctx n} {τ}
--         → Γ ⊢ var x ⇑ τ
--         → lookup x Γ ≡ τ
-- inv-var (var p-≡) = p-≡

-- inv-if  : ∀ {n} {Γ : Ctx n} {t₁ t₂ t₃} {τ}
--         → Γ ⊢ if t₁ then t₂ else t₃ ⇑ τ
--         → Γ ⊢ t₁ ⇓ Bool
--         × Γ ⊢ t₂ ⇑ τ
--         × (∃ λ τ′ → Γ ⊢ t₃ ⇑ τ′
--                   × τ′ ≡ τ)
-- inv-if  (if t₁⇓ then t₂⇑ else t₃⇑ ∣ τ≡τ′) = t₁⇓ , (t₂⇑ , (_ , (t₃⇑ , τ≡τ′)))

-- inv-·   : ∀ {n} {Γ : Ctx n} {t₁ t₂} {τ₂}
--         → Γ ⊢ t₁ · t₂ ⇑ τ₂
--         → (∃ λ τ₁ → Γ ⊢ t₁ ⇑ τ₁ ⇒ τ₂
--                   × Γ ⊢ t₂ ⇓ τ₁)
-- inv-·   (t₁⇑ · t₂⇓) = _ , t₁⇑ , t₂⇓

-- inv-ƛ   : ∀ {n} {Γ : Ctx n} {t} {τ₁ τ₂}
--         → Γ ⊢ ƛ t ⇓ τ₁ ⇒ τ₂
--         → τ₁ ∷ Γ ⊢ t ⇓ τ₂
-- inv-ƛ   (ƛ t⇓) = t⇓

-- inv-inf : ∀ {n} {Γ : Ctx n} {t} {τ}
--         → Γ ⊢ inf t ⇓ τ
--         → (∃ λ τ′ → Γ ⊢ t ⇑ τ′
--                   × τ′ ≡ τ)
-- inv-inf (inf t⇑ τ′≡τ) = _ , t⇑ , τ′≡τ
