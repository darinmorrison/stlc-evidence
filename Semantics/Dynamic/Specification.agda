module Semantics.Dynamic.Specification where

open import Data.Fin
  hiding
    (_+_)
open import Data.Nat
open import Data.Sum
open import Data.Vec
open import Relation.Binary.PropositionalEquality

open import Semantics.Static.Specification
open import Syntax

mutual
  data Env : ∀ {n} → Ctx n → Set where
    []           : Env []

    thunk_env_∷_ : ∀ {n n′} {Γ : Ctx n} {Γ′ : Ctx n′} {t τ}
                 → (ct : Γ′ ⊢[ chk ] t ∶ τ)
                 → (ρ′ : Env Γ′)
                 → (ρ  : Env Γ)
                 → Env (τ ∷ Γ)

    -- Not actually used for now.  For call-by-need in the future.
    value_∷_     : ∀ {n} {Γ : Ctx n} {τ}
                 → (v : Value τ)
                 → (ρ : Env Γ)
                 → Env (τ ∷ Γ)

  data Value : Type → Set where
    ff : Value Bool

    tt : Value Bool

    ƛ  : ∀ {n} {Γ : Ctx n} {t τ₁ τ₂}
      → (ct : τ₁ ∷ Γ ⊢[ chk ] t ∶ τ₂)
      → (ρ  : Env Γ)
      → Value (τ₁ ⇒ τ₂)

_⊕Env_ : ∀ {n m} {Γ : Ctx n} {Δ : Tel n m} → Γ ⊢ Δ → Env Γ → Env (Δ ⊕Ctx Γ)
[]        ⊕Env ρ = ρ
(ct ∷ ⊢Δ) ⊕Env ρ = thunk ct env ρ′ ∷ ρ′
  where
    ρ′ = ⊢Δ ⊕Env ρ

record ThunkClosure (type : Type) : Set where
  constructor thunkClosure
  field
    depth : ℕ
    ctx   : Ctx depth
    rawTm : Chk depth
    wtTm  : ctx ⊢[ chk ] rawTm ∶ type
    env   : Env ctx

lookupEntry : ∀ {n} {Γ : Ctx n} (x : Fin n) → Env Γ → ThunkClosure (lookupType x Γ) ⊎ Value (lookupType x Γ)
lookupEntry zero    (thunk_env_∷_ {n′ = n′} {Γ′ = Γ′} {t = t} ct ρ′ _) = inj₁ (thunkClosure n′ Γ′ t ct ρ′)
lookupEntry zero    (value v       ∷ ρ) = inj₂ v
lookupEntry (suc i) (thunk _ env _ ∷ ρ) = lookupEntry i ρ
lookupEntry (suc i) (value _       ∷ ρ) = lookupEntry i ρ

infix 0 _⊧[_]_⇓_
data _⊧[_]_⇓_ : ∀ {n} {Γ : Ctx n} {τ} → Env Γ → (μ : Mode) → ∀ {t} → Γ ⊢[ μ ] t ∶ τ → Value τ → Set where
  var-thunk : ∀ {n n′} {Γ : Ctx n} {Γ′ : Ctx n′} {ρ : Env Γ} {ρ′ : Env Γ′} {t τ} {x : Fin n} {p-≡ : lookupType x Γ ≡ τ} {ct : Γ′ ⊢[ chk ] t ∶ lookupType x Γ} {v}
            → lookupEntry x ρ ≡ inj₁ (thunkClosure n′ Γ′ t ct ρ′)
            → ρ′ ⊧[ chk ] ct ⇓ v
            → ρ  ⊧[ inf ] var p-≡ ⇓ subst Value p-≡ v

  var-value : ∀ {n} {Γ : Ctx n} {ρ : Env Γ} {τ} {x : Fin n} {p-≡ : lookupType x Γ ≡ τ} {v}
            → lookupEntry x ρ ≡ inj₂ v
            → ρ ⊧[ inf ] var p-≡ ⇓ subst Value p-≡ v

  ff        : ∀ {n} {Γ : Ctx n} {ρ : Env Γ}
            → ρ ⊧[ inf ] ff ⇓ ff

  tt        : ∀ {n} {Γ : Ctx n} {ρ : Env Γ}
            → ρ ⊧[ inf ] tt ⇓ tt

  if-ff     : ∀ {n} {Γ : Ctx n} {ρ : Env Γ} {t₁ t₂ t₃ τ τ′} {ct₁ : Γ ⊢[ chk ] t₁ ∶ Bool} {it₂ : Γ ⊢[ inf ] t₂ ∶ τ} {it₃ : Γ ⊢[ inf ] t₃ ∶ τ′} {τ′≡τ : τ′ ≡ τ} {v₃ : Value τ′}
            → ρ ⊧[ chk ] ct₁ ⇓ ff
            → ρ ⊧[ inf ] it₃ ⇓ v₃
            → ρ ⊧[ inf ] if ct₁ then it₂ else it₃ ∣ τ′≡τ ⇓ subst Value τ′≡τ v₃

  if-tt     : ∀ {n} {Γ : Ctx n} {ρ : Env Γ} {t₁ t₂ t₃ τ τ′} {ct₁ : Γ ⊢[ chk ] t₁ ∶ Bool} {it₂ : Γ ⊢[ inf ] t₂ ∶ τ} {it₃ : Γ ⊢[ inf ] t₃ ∶ τ′} {τ′≡τ : τ′ ≡ τ} {v₂ : Value τ}
            → ρ ⊧[ chk ] ct₁ ⇓ tt
            → ρ ⊧[ inf ] it₂ ⇓ v₂
            → ρ ⊧[ inf ] if ct₁ then it₂ else it₃ ∣ τ′≡τ ⇓ v₂

  _·_       : ∀ {n n′} {Γ : Ctx n} {Γ′ : Ctx n′} {ρ : Env Γ} {ρ′ : Env Γ′} {t₁ t₁′ t₂ τ₁ τ₂} {it₁ : Γ ⊢[ inf ] t₁ ∶ τ₁ ⇒ τ₂} {ct₁ : τ₁ ∷ Γ′ ⊢[ chk ] t₁′ ∶ τ₂} {ct₂ : Γ ⊢[ chk ] t₂ ∶ τ₁} {v : Value τ₂}
            → ρ ⊧[ inf ] it₁ ⇓ ƛ ct₁ ρ′
            → thunk ct₂ env ρ ∷ ρ′ ⊧[ chk ] ct₁ ⇓ v
            → ρ ⊧[ inf ] it₁ · ct₂ ⇓ v

  vdef      : ∀ {n m} {Γ : Ctx n} {Δ : Tel n m} {ρ : Env Γ} {t τ} {⊢Δ : Γ ⊢ Δ} {it : Δ ⊕Ctx Γ ⊢[ inf ] t ∶ τ} {v}
            → ⊢Δ ⊕Env ρ ⊧[ inf ] it ⇓ v
            → ρ ⊧[ inf ] def ⊢Δ ⇒ it ⇓ v

  ƛ         : ∀ {n} {Γ : Ctx n} {ρ : Env Γ} {t τ₁ τ₂} {ct : τ₁ ∷ Γ ⊢[ chk ] t ∶ τ₂}
            → ρ ⊧[ chk ] ƛ ct ⇓ ƛ ct ρ

  inf       : ∀ {n} {Γ : Ctx n} {ρ : Env Γ} {t τ τ′} {it : Γ ⊢[ inf ] t ∶ τ′} {τ′≡τ : τ′ ≡ τ} {v : Value τ′}
            → ρ ⊧[ inf ] it ⇓ v
            → ρ ⊧[ chk ] inf it τ′≡τ ⇓ subst Value τ′≡τ v
