module Semantics.Dynamic.Proof where

open import Category.Monad.Partiality
open Workaround
open import Coinduction
open import Data.Product
open import Data.Sum
open import Relation.Binary.PropositionalEquality

open import Semantics.Dynamic.Specification
open import Semantics.Static.Specification
open import Syntax

-- See 'eval' below...

evalP : ∀ {n} {Γ : Ctx n} {μ : Mode} {t : term μ n} {τ}
      → (μt : Γ ⊢[ μ ] t ∶ τ)
      → (ρ  : Env Γ)
      → (Σ (Value τ) λ v → ρ ⊧[ μ ] μt ⇓ v) ⊥P
evalP ff ρ = now (, ff)
evalP tt ρ = now (, tt)
evalP (if  ct₁
      then it₂
      else it₃
        ∣ τ′≡τ) ρ = evalP ct₁ ρ >>= ⟦if⟧
  where
    ⟦if⟧ : ∃ (λ v → ρ ⊧[ chk ] ct₁ ⇓ v)
         → ∃ (λ v → ρ ⊧[ inf ] if ct₁ then it₂ else it₃ ∣ τ′≡τ ⇓ v) ⊥P
    ⟦if⟧ (ff , ct₁⇓)  = evalP it₃ ρ >>= λ Σv
                      → now (subst Value τ′≡τ (proj₁ Σv) , if-ff ct₁⇓ (proj₂ Σv))
    ⟦if⟧ (tt , ct₁⇓)  = evalP it₂ ρ >>= λ Σv
                      → now (proj₁ Σv , if-tt ct₁⇓ (proj₂ Σv))
evalP (var {x = x} p-≡) ρ with inspect (lookupEntry x ρ)
... | inj₁ (thunkClosure depth ctx rawTm wtTm ρ′) with-≡ eq = later (♯ evalP wtTm ρ′) >>= λ Σv → now (subst Value p-≡ (proj₁ Σv) , var-thunk eq (proj₂ Σv))
... | inj₂ v                                      with-≡ eq =                                    now (subst Value p-≡ v          , var-value eq)
evalP (_·_ {τ₁ = τ₁} {τ₂ = τ₂} it₁ ct₂) ρ = evalP it₁ ρ >>= ⟦·⟧
  where
    ⟦·⟧ : Σ (Value (τ₁ ⇒ τ₂)) (λ v → ρ ⊧[ inf ] it₁ ⇓ v)
          → (Σ (Value τ₂) (λ v → ρ ⊧[ inf ] it₁ · ct₂ ⇓ v)) ⊥P
    ⟦·⟧ (ƛ ct ρ′ , it₁⇓) = later (♯ evalP ct (thunk ct₂ env ρ ∷ ρ′)) >>= λ Σv → now (proj₁ Σv , (it₁⇓ · proj₂ Σv))
evalP (def ⊢Δ ⇒ it) ρ = evalP it (⊢Δ ⊕Env ρ) >>= λ Σv → now (proj₁ Σv , vdef (proj₂ Σv))
evalP (ƛ ct)        ρ = now (ƛ ct ρ , ƛ)
evalP (inf it τ′≡τ) ρ = evalP it  ρ >>= λ Σv
                      → now (subst Value τ′≡τ (proj₁ Σv), inf (proj₂ Σv))

--------------------------------------------------------------------------------
-- Partial type soundness (using the productivity checking workaround)
--------------------------------------------------------------------------------

eval : ∀ {n} {Γ : Ctx n} {μ : Mode} {t : term μ n} {τ}
     → (μt : Γ ⊢[ μ ] t ∶ τ)
     → (ρ  : Env Γ)
     → (Σ (Value τ) λ v → ρ ⊧[ μ ] μt ⇓ v) ⊥
eval μt ρ = ⟦ evalP μt ρ ⟧P

