module Examples where

open import Category.Monad.Partiality
open import Data.Fin
open import Data.Maybe
open import Data.Nat
open import Data.Product
open import Data.Sum
open import Data.Vec
open import Relation.Binary.PropositionalEquality
open import Syntax
open import Semantics.Static.Proof
open import Semantics.Static.Specification
open import Semantics.Dynamic.Proof
open import Semantics.Dynamic.Specification

-- evaluates in 4 steps
prog : Inf 0
prog = def Δ ⇒ var zero · inf (var zero · inf tt · inf tt) · inf ff
  where
    Δ = ƛ (ƛ (inf (if inf (var zero) then var (suc zero) else ff))) ∶ Bool ⇒ Bool ⇒ Bool
      ∷ []

run : ∀ {n} {Γ : Ctx n} (t : Inf n) (ρ : Env Γ) → ℕ → Maybe (Σ Type Value)
run {Γ = Γ} t ρ k    with infer Γ t
run {Γ = Γ} t ρ k
  | inj₁   (τ , it ) with run (eval it ρ) for k steps
run {Γ = Γ} t ρ k
  | inj₁   (τ , it )
  | now    (v , it⇓) = just (τ , v)
run {Γ = Γ} t ρ k
  | inj₁   (τ , it)
  | later  _         = nothing
run {Γ = Γ} t ρ k
  | inj₂   _         = nothing

